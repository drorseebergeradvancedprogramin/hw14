#include "Arrow.h"



void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}

/*
c'tor for Arrow
input: two points the type and the name
*/
Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name): Shape(name,type),_p1(a),_p2(b)
{
}

// the d'tor of arrow
Arrow::~Arrow()
{
}

/*
returns the area of an Arrow(0)
input: none
output: 0
*/
double Arrow::getArea() const
{
	return 0;
}

/*
returns the distance between the two points
input: none
output: the distance 
*/
double Arrow::getPerimeter() const
{
	return _p1.distance(_p2);
}

/*
moves the arrow with a point
input: a point
output: none
*/
void Arrow::move(const Point& other)
{
	_p1 += other;
	_p2 += other;
}