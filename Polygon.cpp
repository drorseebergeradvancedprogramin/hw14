#include "Polygon.h"

/*================done?==============*/

/*
c'tor of polygon
input: name and type
*/
Polygon::Polygon(const string& type, const string& name):Shape(name,type)
{
}

/*
d'tor of polygon
*/
Polygon::~Polygon()
{
	MessageBeep(MB_OK);
}

/*
moves the polygon with a point
input: other point
output: none
*/
void Polygon::move(const Point& other)
{
	for (int i = 0; i < _points.size(); i++)
	{
		_points[i] += other;
	}
}