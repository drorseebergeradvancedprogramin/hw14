#include "Triangle.h"



void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

/*
c'tor of Triangle
input: three points, name and type
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) :Polygon(type,name)
{
	// if not on the same axis
	if (!(a.getX() == b.getX() && a.getX() == c.getX()) && !(a.getY() == b.getY() && a.getY() == c.getY()))
	{	
			_points.push_back(*(new Point(a)));
			_points.push_back(*(new Point(b)));
			_points.push_back(*(new Point(c)));
		
	}
	else
	{
		throw invalid_argument("points on the same axis");
	}
	
}

//d'tor of Triangle
Triangle::~Triangle()
{
	//deleting the points
	for (int i = _points.size(); i > 0; i--)
	{
		Point temp = _points[i-1];
		_points.pop_back();
		delete &temp;
	}
}

/*
returns the area of the triangle
input: none
output: the area
*/
double Triangle::getArea() const
{
	double x1, x2, x3;
	double y1, y2, y3;
	x1 = _points[0].getX();
	x2 = _points[1].getX();
	x3 = _points[2].getX();

	y1 = _points[0].getY();
	y2 = _points[1].getY();
	y3 = _points[2].getY();

	double area = 0;
	// a formula to find the area of a triangle with three points
	area = (abs(x1*(y3 - y2) + x2*(y1 - y3) + x3*(y2 - y1))) / 2;
	return area;
}

/*
returns the perimeter of the triangle
input: none
output: the perimeter
*/
double Triangle::getPerimeter() const
{
	double perim = 0;

	perim += _points[0].distance(_points[1]);
	perim += _points[1].distance(_points[2]);
	perim += _points[2].distance(_points[0]);
	
	return perim;
}