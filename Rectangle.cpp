#include "Rectangle.h"


/*================done?==============*/

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


/*
c'tor for Rectangle
input: top right corner,length,width,type,name
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name):Polygon(type,name)
{
	if (width > 0 && length > 0)
	{
		_len = length;
		_width = width;
		
		//pushing all the points to the vector
		_points.push_back(Point(a.getX(), a.getY()));
		_points.push_back(Point(a.getX() + width, a.getY()+length));
	}
	else
	{
		throw invalid_argument("negative or zero width or length");
	}
}

//d'tor for rectangle
myShapes::Rectangle::~Rectangle()
{
	MessageBeep(MB_OK);
}

/*
returns the area of the ractangle
input: none
output: the area
*/
double myShapes::Rectangle::getArea() const
{
	return _len*_width;
}

/*
returns the Perimeter of the ractangle
input: none
output: the Perimeter
*/
double myShapes::Rectangle::getPerimeter() const
{
	return (_len * 2) + (_width * 2);
}