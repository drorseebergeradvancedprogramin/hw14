#include "Circle.h"

/*================done==============*/


void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}

/*
c'tor of Circle
input: the center point, the radius, the type and name
*/
Circle::Circle(const Point& center, double radius, const string& type, const string& name) : _center(center),Shape(name, type)
{
	_radius = radius;
}

/*
d'tor of circle
*/
Circle::~Circle()
{
}

/*
returns the center of the circle 
input: none
output: the circle point
*/
const Point& Circle::getCenter() const
{
	return _center;
}

/*
returns the radius of the circle
input: none
output: the radius
*/
double Circle::getRadius() const
{
	return _radius;
}

/*
returns the area of the circle
input: none
output: the area
*/
double Circle::getArea() const
{
	double area = PI * _radius* _radius;

	return area;
}

/*
calculates the perimeter and returns it
input: none
output: the perimeter
*/
double Circle::getPerimeter() const
{
	double perim = PI * 2 *_radius;
	return perim;
}

/*
moves the center point
input: a point
output: none
*/
void Circle::move(const Point& other)
{
	_center += other;
}