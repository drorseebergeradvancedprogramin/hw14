#include "Shape.h"


/*================done==============*/

/*
c'tor of shape
input: the name of the shape and the type
*/
Shape::Shape(const string& name, const string& type) : _name(name), _type(type)
{
}

/*
prints the name and type
no input or output
*/
void Shape::printDetails() const
{
	cout << "\nName: " << _name << "\nType: " << _type << "\n";
}

/*
returns the name of shape
input: none
output: the name
*/
string Shape::getName() const
{
	return _name;
}

/*
returns the type of shape
input: none
output: the type
*/
string Shape::getType() const
{
	return _type;
}