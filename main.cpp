#include "Menu.h"

void main()
{
	bool exit = false;
	Menu mainMenu;

	while (!exit)
	{
		mainMenu.printMainMenu();
		try
		{
			exit = mainMenu.getChoice();
		}
		catch (...)
		{
			cout << "\n ERROR\n";
			system("pause");
		}
		
		system("cls");
	}
}