#include "Point.h"
#include <math.h>

/*================done==============*/

/*
c'tor of point
input: x,y valuse
*/
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

/*
copy c'tor of point
input: other point
*/
Point::Point(const Point& other)
{
	_x = other.getX();
	_y = other.getY();
}

//d'tor of Point
Point::~Point()
{

}

/*
returns the x value
input: none
output: x value
*/
double Point::getX() const
{
	return _x;
}

/*
returns the y value
input: none
output: y value
*/
double Point::getY() const
{
	return _y;
}

/*
adds two points and returns a point with the added values
input: other point
output: a point
*/
Point Point::operator+(const Point& other) const
{
	Point NewPoint(_x + other.getX() , _y + other.getY());
	return NewPoint;
}

/*
adds another point to this point
input: other point
output: referace to this point
*/
Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	
	return *this;
}

/*
caclulats the distance between two points
input: other point
output: the distance
*/
double Point::distance(const Point& other) const
{
	double distance = 0;
	double xSide = 0;
	double ySide = 0;

	xSide = this->getX() - other.getX();
	ySide = this->getY() - other.getY();

	distance = (xSide * xSide) + (ySide * ySide);
	distance = sqrt(distance);

	return distance;
}