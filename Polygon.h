#pragma once

#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
#include <Windows.h>
//#include "Cimg.h"

using namespace std;

class Polygon : public Shape
{
public:
	Polygon(const string& type, const string& name);
	virtual ~Polygon();

	// override functions if need (virtual + pure virtual)
	virtual void move(const Point& other);
protected:
	vector<Point> _points;
};