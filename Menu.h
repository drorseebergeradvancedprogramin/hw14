#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

//MENU
#define OPTION_NUM 3
#define CREATE_SHAPE 0
#define CHANGE_SHAPE 1
#define DELETE_ALL 2
#define EXIT 3

//CREATE MENU
#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3

//CHANGE SHAPE
#define MOVE_SHAPE 0
#define INFO_SHAPE 1
#define DELETE_SHAPE 2

#define BIG_NUM 10000

class Menu
{
public:

	Menu();
	~Menu();

	// more functions..

	bool getChoice();
	void printMainMenu();

private: 
	void drawAll();
	void createNewShape();
	void deleteAll();
	void deleteOne(int index);
	void changeShape();
	vector<Shape*> _shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	Shape* createCirlce();
	Shape* createArrow();
	Shape* createTriangle();
	Shape* createRectangle();
	void getPointAxis(double*);
};

