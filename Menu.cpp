#include "Menu.h"


/*
c'tor of menu
*/
Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

//d'tor of Menu
Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;

	//cleaning the shape vector
	if (_shapes.size() > 0)
	{
		for (int i = 0; i < _shapes.size()-1; i++)
		{
			delete _shapes[i];
		}
	}
}

//prints the menu
//no input or output
void Menu::printMainMenu()
{
	std::cout << "Enter a Choice:\n0- Add shape\n1- Modify shape or get information on a shape\n2-Delete all shapes\n3-Exit\n";
}

/*
gets a choice from the user and calls a function according to the choice
input: none
output: none
*/
bool Menu::getChoice()
{
	int _choice = -1;

	while (_choice < 0 || _choice > OPTION_NUM)
	{
		//getting input
		_choice = -1;
		std::cout << "\nChoice: ";
		std::cin >> _choice;
	}

	//calling the function according to the choice
	switch (_choice)
	{
		case CREATE_SHAPE:
		{
			createNewShape();		
			break;
		}

		case CHANGE_SHAPE:
		{
			changeShape();
			break;
		}

		case DELETE_ALL:
		{
			deleteAll();
			break;
		}
		
		case EXIT:
		{
			deleteAll();
			return true;
			break;
		}
			
		default: return true;

	}
	return false;
}

/*
creates a new shape and adds it to the shape vector
input: none
output: none
*/
void Menu::createNewShape()
{
	system("cls");
	std::cout << "0 - add circle\n1 - add arrow\n2 - add triangle\n3 - add rectangle\n";
	int newShapeChoice = -1;
	Shape* newShape;

	//getting input on which shape to create
	while (newShapeChoice < 0 || newShapeChoice > OPTION_NUM)
	{
		newShapeChoice = -1;
		std::cin >> newShapeChoice;
	}

	//creating the shape
	switch (newShapeChoice)
	{
		case CIRCLE:
		{
			newShape = createCirlce();
			break;
		}

		case ARROW:
		{
			newShape = createArrow();
			break;
		}

		case TRIANGLE:
		{
			newShape = createTriangle();
			break;
		}

		case RECTANGLE:
		{
			newShape = createRectangle();
			break;
		}
		default: throw "shape create switch error";

	}

	//adding the shape to the vector
	_shapes.push_back(newShape);

	//drawing it
	newShape->draw(*_disp,*_board);
}

/*
deletes all the shapes in the vector
has no input or output
*/
void Menu::deleteAll()
{
	for (int i = _shapes.size()-1; i >= 0; i--)
	{
		deleteOne(i);
	}
}

/*
deletes one shape in the vector
input: the index of the shape
output: none
*/
void Menu::deleteOne(int index)
{
	
	_shapes[index]->clearDraw(*_disp, *_board);
	delete _shapes[index];
	_shapes.erase(_shapes.begin() +index);
}

/*
deletes/gets info/moves a shape
no input or output;
*/
void Menu::changeShape()
{
	int choice = -1;
	int index = -1;
	const int CHANGE_OPTION_NUM = 2;
	if (_shapes.size() > 0)
	{
		
		//choosing the shape
		for (int i = 0; i < _shapes.size(); i++)
		{
			cout << i << " - " << _shapes[i]->getName() << "(" << _shapes[i]->getType() << ")\n";
		}
		//input for choice
		while (index < 0 || index >= _shapes.size())
		{
			cout << "choice: ";
			cin >> index;
		}

		//printing the sub menu
		cout << "\n0 - move the shape\n1 - get details\n2 - remove the shape\n";
		while (choice < 0 || choice > CHANGE_OPTION_NUM)
		{
			cout << "choice: ";
			cin >> choice;
		}

		//reacting to the choice
		switch(choice)
		{
			case MOVE_SHAPE:
			{
				//geting the point
				double moveX = 0;
				double moveY = 0;
				cout << "\nenter x value to move: ";
				cin >> moveX;
				cout << "enter y value to move: ";
				cin >> moveY;

				//moving the shape and drawing everything again
				
				_shapes[index]->clearDraw(*_disp, *_board);
				_shapes[index]->move(Point(moveX, moveY));
				drawAll();
				break;
			}

			case INFO_SHAPE:
			{
				cout << "\nName: " << _shapes[index]->getName() << endl;
				cout << "Type: " << _shapes[index]->getType() << endl;
				cout << "Area: " << _shapes[index]->getArea() << endl;
				cout << "Perimeter: " << _shapes[index]->getPerimeter() << endl;
				system("pause");
				break;
			}

			case DELETE_SHAPE:
			{
				deleteOne(index);
				break;
			}

			default: throw "Error";
		}
	}
}

/*
draws all the shapes
no input or output
*/
void Menu::drawAll()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		_shapes[i]->draw(*_disp, *_board);
	}
}

/*
creates a circle and returns the pointer to it
input: none
output: the pointer
*/
Shape* Menu::createCirlce()
{
	
	double x = 0;
	double y = 0;
	double radius = 0;
	string name;

	//getting input on the circle
	cout << "enter x: ";
	getPointAxis(&x);
	cout << "enter y: ";
	getPointAxis(&y);
	cout << "enter Radius: ";
	getPointAxis(&radius);
	cout << "enter the name of the shape: ";
	cin >> name;
	system("cls");
	Shape* newCircle = new Circle(Point(x, y), radius, "Circle", name);
	return newCircle;
}

/*
creates an Arrow and returns the pointer to it
input: none
output: the pointer
*/
Shape* Menu::createArrow()
{
	double x1, x2, y1, y2;
	string name;

	cout << "enter x1: ";
	getPointAxis(&x1);
	cout << "enter y1: ";
	getPointAxis(&y1);
	cout << "enter x2: ";
	getPointAxis(&x2);
	cout << "enter y2: ";
	getPointAxis(&y2);
	cout << "enter the name of the arrow: ";
	cin >> name;

	Shape* newArrow = new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", name);

	system("cls");
	return newArrow;
	
}

/*
creates a Triangle and returns the pointer to it
input: none
output: the pointer
*/
Shape* Menu::createTriangle()
{
	double x1, x2, y1, y2,x3,y3;
	bool flag = true;
	string name;
	while (flag)
	{
		system("cls");
		flag = false;
		cout << "enter x1: ";
		getPointAxis(&x1);
		cout << "enter y1: ";
		getPointAxis(&y1);
		cout << "enter x2: ";
		getPointAxis(&x2);
		cout << "enter y2: ";
		getPointAxis(&y2);
		cout << "enter x3: ";
		getPointAxis(&x3);
		//checking if all 3 points are on the same line
		if (x1 == x2 && x2 == x3)
		{
			std::cout << "the three points can't have the same x!\n";
			flag = true;
			system("pause");
			continue;
		}
		cout << "enter y3: ";
		getPointAxis(&y3);
		//checking if all 3 points are on the same line
		if (y1 == y2 && y2 == y3)
		{
			std::cout << "the three points can't have the same y!\n";
			flag = true;
			system("pause");
			continue;
		}
	}
	
	cout << "enter the name of the triangle: ";
	cin >> name;

	Shape* newTriangle = new Triangle(Point(x1, y1), Point(x2, y2),Point(x3,y3), "Triangle", name);

	system("cls");
	return newTriangle;
}

/*
creates a Rectangle and returns the pointer to it
input: none
output: the pointer
*/
Shape* Menu::createRectangle()
{
	
	double x1, y1,width, length;
	string name;
	cout << "enter x of top left corner: ";
	getPointAxis(&x1);
	cout << "enter y of top left corner: ";
	getPointAxis(&y1);
	cout << "enter width of rectangle: ";
	getPointAxis(&width);
	cout << "enter length of rectangle: ";
	getPointAxis(&length);
	cout << "enter name of rectangle: ";
	cin >> name;

	Shape* newRectangle = new myShapes::Rectangle(Point(x1,y1),length,width,"Rectangle",name);
	system("cls");
	return newRectangle;
}


//gets a number from user
void Menu::getPointAxis(double* var)
{
	double temp = 0;
	cin >> temp;
	while (temp < 0)
	{
		std::cout << "x/y axis can't be negative!\ntry again: ";
		//cleaning buffer
		cin.clear();
		cin.ignore(BIG_NUM, '\n');
		//getting input again
		cin >> temp;
	}
	//cleaning buffer
	cin.clear();
	cin.ignore(BIG_NUM, '\n');

	//puting the resault in var
	*var = temp;
}

